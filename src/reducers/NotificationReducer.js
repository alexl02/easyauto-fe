const INITIAL_STATE = {
    notificationPermission: false,
    notificationsEnabled: false
};
 
export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case "SET_NOTIFICATION_PERMISSION":
            return {...state, notificationPermission: action.payload};

        case "SET_NOTIFICATION_ENABLED":
            return {...state, notificationEnable: action.payload};
            
            default: return state;
    }
}