import { httpPost, httpGetError, httpGet } from '../http';

export const getLogsAction = () => (dispatch, getState) => {
    dispatch({
        type: 'LOG_LOADING'
    });

    const { session } = getState().SessionReducer;

    try{
        httpGet("/log/user/" + session.user._id, session._id).then(
            resp => {
                
                if(resp.data){
                    dispatch({
                        type: 'SET_LOGS',
                        payload: resp.data
                    });
                }
                
            }
        ).catch(
            error => {
                let payloadError = httpGetError(error);
                
                dispatch({
                    type: "LOG_ERROR",
                    payload: payloadError
                });
            }
        );
    } catch (error) {
        console.log(error);
    }
}

export const getCompaniesAction = () => (dispatch, getState) => {

    dispatch({
        type: 'LOG_LOADING'
    });

    const { session } = getState().SessionReducer;

    try{
        httpGet("/log/user/" + session.user._id, session._id).then(
            resp => {
                
                if(resp.data){

                    dispatch({
                        type: 'SET_COMPANIES',
                        payload: resp.data
                    });
                    
                }
                return 0;
            }
        ).catch(
            error => {
                let payloadError = httpGetError(error);
                
                dispatch({
                    type: "LOG_ERROR",
                    payload: payloadError
                });
                return 0;
            }
        );
    } catch (error) {
        console.log(error);
        return 0;
    }
    
}

export const getPosServers = () => (dispatch, getState) => {
    
    if(getState().LogReducer.company._id.length > 0){

        const { company } = getState().LogReducer;
                
        dispatch({
            type: 'LOG_LOADING'
        });
    
        const { session } = getState().SessionReducer;
    
        dispatch({
            type: 'SET_LOG',
            payload: company.log
        });
    
        try{
            httpGet("/log/posServers/" + company.log._id, session._id).then(
                resp => {
    
                    if (resp.data){

                        dispatch({
                            type: 'SET_POS_SERVERS',
                            payload: resp.data
                        });
                    }
                    
                }
            ).catch(
                error => {
                    let payloadError = httpGetError(error);
                    
                    dispatch({
                        type: "LOG_ERROR",
                        payload: payloadError
                    });
                }
            );
        } catch (error) {
            console.log(error);
        }
    }
}

export const getFasesAction = () => (dispatch, getState) => {
    
    const { pos } = getState().LogReducer;
    
    if(pos._id.length > 0){

        dispatch({
            type: 'LOG_LOADING'
        });
    
        const { session } = getState().SessionReducer;
    
        dispatch({
            type: 'SET_POS',
            payload: pos
        });
    
        try{
            httpGet("/log/logPosId/" + pos._id, session._id).then(
                resp => {
                    if (resp.data){
    
                        dispatch({
                            type: 'SET_FASES',
                            payload: resp.data.fases
                        });
                    }
                    
                }
            ).catch(
                error => {
                    let payloadError = httpGetError(error);
                    
                    dispatch({
                        type: "LOG_ERROR",
                        payload: payloadError
                    });
                }
            );
        } catch (error) {
            console.log(error);
        }
    }
}

export const getFaseAction = (fase) => (dispatch, getState) => {
       
    dispatch({
        type: 'LOG_LOADING'
    });

    const { session } = getState().SessionReducer;

    dispatch({
        type: 'SET_FASE',
        payload: fase
    });

    try{
        httpGet("/log/fase/" + fase._id, session._id).then(
            resp => {

                if (resp.data){

                    dispatch({
                        type: 'SET_FASE',
                        payload: resp.data
                    });
                }
            }
        ).catch(
            error => {
                let payloadError = httpGetError(error);
                
                dispatch({
                    type: "LOG_ERROR",
                    payload: payloadError
                });
            }
        );
    } catch (error) {
        console.log(error);
    }
}

export const setCompanyAction = (company) => (dispatch, getState) => {
    dispatch({
        type: 'SET_COMPANY',
        payload: company
    });
}

export const setPosAction = (pos) => (dispatch, getState) => {
    dispatch({
        type: 'SET_POS',
        payload: pos
    });
}

export const refreshAction = () => (dispatch, getState) => {
    getCompaniesAction();
    getPosServers();
    getFasesAction();
}