import React from 'react';
import { CheckCircleTwoTone, CloseCircleTwoTone, MinusCircleTwoTone, LoadingOutlined } from '@ant-design/icons';
import './Box.css'

const Box = (props) => {
    return(
        <div className='box-container'>
            <div className='box-text'>
                { props.data.name ? props.data.name : props.data.fase }
            </div>
            <div className='box-icon'>
                { props.data.state === 0 ? <LoadingOutlined spin='true' style={{ fontSize: 'x-large' }} /> 
                    : props.data.state === 1 ? <CheckCircleTwoTone style={{ fontSize: 'x-large' }} twoToneColor="#52c41a" />
                        : props.data.state === 2 ? <MinusCircleTwoTone style={{ fontSize: 'x-large' }} />
                            : <CloseCircleTwoTone style={{ fontSize: 'x-large' }} twoToneColor="#eb2f96" />
                }
            </div>
        </div>
    );
}

export default Box;