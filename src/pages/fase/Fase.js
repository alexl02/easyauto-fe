import React from 'react';
import FaseView from './FaseView';

const Fase = (props) => {
    const { fase, companySelected, posServerSelected } = props.history.location.state;
    return <FaseView fase={fase} companySelected={companySelected} posServerSelected={posServerSelected} />
}

export default Fase;