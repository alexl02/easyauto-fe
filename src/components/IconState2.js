import React from 'react';
import { ReloadOutlined, EyeOutlined, CheckCircleTwoTone, CloseCircleTwoTone, MinusCircleTwoTone, LoadingOutlined, PauseCircleTwoTone, WarningTwoTone } from '@ant-design/icons';

const IconState2 = (props) => {
    return(
        <div className="icon-state-container">
            { 
                props.state === 0 ? <LoadingOutlined spin='true' style={{ fontSize: '40px' }} /> 
                    : props.state === 1 ? <CheckCircleTwoTone style={{ fontSize: '40px' }} twoToneColor="#52c41a" />
                        : props.state === 2 ? <MinusCircleTwoTone style={{ fontSize: '40px' }} twoToneColor="#a6a6a6" />
                            : props.state === -1 ? <CloseCircleTwoTone style={{ fontSize: '40px' }} twoToneColor="#ff0000" />
                                : props.state === 3 ? <PauseCircleTwoTone style={{ fontSize: '40px' }} />
                                    : <WarningTwoTone style={{ fontSize: '40px' }} twoToneColor="#ff9933" />

            }
        </div>
    );
}

export default IconState2;